# Executables (local)
DOCKER_COMP = @cd docker && docker-compose -f

# Docker containers
EXEC_PHP = $(DOCKER_COMP) exec -T php

EXEC_CONTAINER = @cd docker && docker exec -it
# Executables
PHP      = $(EXEC_PHP) php
COMPOSER = $(EXEC_PHP) composer
SYMFONY  = $(EXEC_PHP) bin/console

# Environnement(local|prod)
LOCAL = $(DOCKER_COMP) -f docker-compose-local.yaml

# Misc
.DEFAULT_GOAL = help
.PHONY        = help build up start down logs sh composer vendor sf cc

## —— 🎵 🐳 The Symfony-docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build: ## Builds the Docker images
	@$(eval env ?=)
	@$(DOCKER_COMP) $(env) build

up: ## Start the docker hub in detached mode (no logs)
	@$(eval env ?=)
	@$(DOCKER_COMP) $(env) up --detach

start: build up ## Build and start the containers

down: ## Stop the docker hub
	@$(eval env ?=)
	@$(DOCKER_COMP) $(env) down --remove-orphans

logs: ## Show live logs
	@$(eval env ?=)
	@$(DOCKER_COMP) $(env) logs --tail=0 --follow

sh: ## Connect to the PHP FPM container
	@$(eval container ?=)
	@$(eval type ?=)
	@$(EXEC_CONTAINER) $(container) $(type)

