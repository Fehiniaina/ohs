# Build project
```bash
$ cd docker && docker-compose -f docker-compose.yaml -f docker-compose-local.yaml up --build
```
# Up project
```bash
$ cd docker && docker-compose -f docker-compose.yaml -f docker-compose-local.yaml up -d
```